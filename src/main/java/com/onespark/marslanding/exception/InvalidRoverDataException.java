package com.onespark.marslanding.exception;

public class InvalidRoverDataException extends Exception {
    public InvalidRoverDataException(String message) {
        super(message);
    }
}
