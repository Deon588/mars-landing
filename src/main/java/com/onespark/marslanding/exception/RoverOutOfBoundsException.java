package com.onespark.marslanding.exception;

public class RoverOutOfBoundsException extends Exception {
    public RoverOutOfBoundsException(String message) {
        super(message);
    }
}
