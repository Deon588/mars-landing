package com.onespark.marslanding;

import com.onespark.marslanding.model.MarsRover;
import com.onespark.marslanding.exception.InvalidRoverDataException;
import com.onespark.marslanding.exception.RoverOutOfBoundsException;
import com.onespark.marslanding.model.Position;
import com.onespark.marslanding.model.Direction;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.Pattern;

public class MarsLandingApplication {

    public BufferedReader readData(String fileName) {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader("./" + fileName));
        } catch (FileNotFoundException e) {
            System.out.println("File " + fileName + " not found in directory" + System.getProperty("user.dir") + " please try again.");
        } catch (IOException e) {
            throw e;
        }
        return in;
    }

    public void processData(BufferedReader in) throws InvalidRoverDataException, RoverOutOfBoundsException, IOException {
        String str = in.readLine();
        int line = 1;
        MarsRover rover = new MarsRover();
        int[] maxCoords = null;
        while (str != null) {
            if (line == 1) {
                if (validateMAxCoordsRead(str)) {
                    maxCoords = parseMaxPosition(str);
                } else {
                    throw new InvalidRoverDataException("Data format error at line " + line);
                }
                str = in.readLine();
                line += 1;
            } else if (line % 2 == 0) {
                if (validateInitialPositionRead(str)) {
                    rover.setPosition(this.parseInitialPosition(str));
                } else {
                    throw new InvalidRoverDataException("Data format error at line " + line);
                }
                str = in.readLine();
                line += 1;
            } else if (line % 2 > 0 && line != 1) {
                if (validateMoves(str)) {
                    rover.setMaxPosition(maxCoords);
                    rover.moveOnRoute(this.parseMoves(str));
                    System.out.println(rover.getPosition().toString());
                } else {
                    throw new InvalidRoverDataException("Data format error at line " + line);
                }
                str = in.readLine();
                line += 1;
                rover = new MarsRover();
            }
        }
    }

    public int[] parseMaxPosition(String data) {
//        String[] coords = data.split(" ");
        return Arrays.stream(data.split(" ")).mapToInt(Integer::parseInt).toArray();
    }

    private Position parseInitialPosition(String data) {
        String[] pos = data.split(" ");
        return new Position(Integer.parseInt(pos[0]), Integer.parseInt(pos[1]), Direction.valueOf(pos[2]));
    }

    private String[] parseMoves(String data) {
        String[] moves = data.split("(?!^)");
        return moves;
    }

    public boolean validateInitialPositionRead(String position) {
        String regex = "^[0-9]+\\s[0-9]+\\s+[ENWS]+$";
        return Pattern.matches(regex, position);
    }

    public boolean validateMAxCoordsRead(String coords) {
        String regex = "^[0-9]+\\s[0-9]+$";
        return Pattern.matches(regex, coords);
    }

    private boolean validateMoves(String moves) {
        String regex = "^[LRM]+$";
        return Pattern.matches(regex, moves);
    }

    public static void main(String[] args) throws Exception {
        MarsLandingApplication marsLanding = new MarsLandingApplication();
        String fileName = null;
        BufferedReader in = null;
        System.out.println("Mars Landing \n");
        do {
            System.out.println("Please enter data file name including extension (relative to project root directory when running from IDE otherwise make sure the data file is in the same directory as the jar when running application) \n");
            Scanner input = new Scanner(System.in);
            fileName = input.nextLine();

            in = marsLanding.readData(fileName);
        } while (in == null);

        marsLanding.processData(in);
    }
}
