package com.onespark.marslanding.model;

public enum Direction {
    E,
    N,
    S,
    W
}
