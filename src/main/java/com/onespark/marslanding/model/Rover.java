package com.onespark.marslanding;

import com.onespark.marslanding.exception.RoverOutOfBoundsException;

public interface Rover {
    void moveOnRoute(String[] moves) throws RoverOutOfBoundsException;
}
