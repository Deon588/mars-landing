package com.onespark.marslanding.model;

import com.onespark.marslanding.Rover;
import com.onespark.marslanding.exception.RoverOutOfBoundsException;
import com.onespark.marslanding.model.Direction;
import com.onespark.marslanding.model.Position;

import java.util.List;

public class MarsRover implements Rover {
    private Position position;
    private int[] maxPosition;

    public MarsRover() {
    }

    private void turnLeft() {
        switch (this.position.getDirection()) {
            case N:
                this.position.setDirection(Direction.W);
                break;
            case S:
                this.position.setDirection(Direction.E);
                break;
            case E:
                this.position.setDirection(Direction.N);
                break;
            case W:
                this.position.setDirection(Direction.S);
                break;
            default:
                break;
        }
    }

    private void turnRight() {
        switch(this.position.getDirection()) {
            case N:
                this.position.setDirection(Direction.E);
                break;
            case E:
                this.position.setDirection(Direction.S);
                break;
            case S:
                this.position.setDirection(Direction.W);
                break;
            case W:
                this.position.setDirection(Direction.N);
                break;
            default:
                break;
        }
    }

    private void moveForward() throws RoverOutOfBoundsException {
        switch(this.position.getDirection()) {
            case N:
                this.validateMove(this.position.getX(), this.position.getY() + 1);
                this.position.setY(this.position.getY() + 1);
                break;
            case S:
                this.validateMove(this.position.getX(), this.position.getY() - 1);
                                this.position.setY(this.position.getY() - 1);
                break;
            case W:
                this.validateMove(this.position.getX() - 1, this.position.getY());
                this.position.setX(this.position.getX() - 1);
                break;
            case E:
                this.validateMove(this.position.getX() + 1, this.position.getY());
                this.position.setX(this.position.getX() + 1);
                break;
            default:
                break;
        }
    }

    @Override
    public void moveOnRoute(String[] moves) throws  RoverOutOfBoundsException {
        for (String move: moves) {
            switch(move) {
                case "M":
                    this.moveForward();
                    break;
                case "L":
                    this.turnLeft();
                    break;
                case "R":
                    this.turnRight();
                    break;
            }
        }
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public int[] getMaxPosition() {
        return maxPosition;
    }

    public void setMaxPosition(int[] maxPosition) {
        this.maxPosition = maxPosition;
    }

    private void validateMove(int x, int y) throws RoverOutOfBoundsException {
        if (x > maxPosition[0]) {
            throw new RoverOutOfBoundsException("Invalid move:  rover reached right edge of plateau.");
        }
        if (x < 0) {
            throw new RoverOutOfBoundsException("Invalid move:  rover reached left edge of plateau.");
        }
        if (y > maxPosition[1]) {
            throw new RoverOutOfBoundsException("Invalid move:  rover reached top edge of plateau.");
        }
        if (y < 0) {
            throw new RoverOutOfBoundsException("Invalid move:  rover reached bottom edge of plateau.");
        }

    }
}
