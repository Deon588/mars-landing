package com.onespark.marslanding.model;

import com.onespark.marslanding.exception.InvalidRoverDataException;
import com.onespark.marslanding.exception.RoverOutOfBoundsException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class MarsRoverTests {
    private final MarsRover rover = new MarsRover();

    @BeforeEach()
    public void setupRover() {
        int[] maxCoords = {5, 5};
        Position pos = new Position(2, 2, Direction.N);
        rover.setMaxPosition(maxCoords);
        rover.setPosition(pos);
    }

    @Test()
    public void whenRoverFacesNorthAndTurnsLeftOnceRoverShouldFaceWest() throws RoverOutOfBoundsException {
        String[] moves = {"L"};
        rover.moveOnRoute(moves);
        assertThat(rover.getPosition().getDirection(), is(Direction.W));
    }

    @Test()
    public void whenRoverFacesNorthAndTurnsLeftTwiceRoverShouldFaceSouth() throws RoverOutOfBoundsException {
        String[] moves = {"L", "L"};
        rover.moveOnRoute(moves);
        assertThat(rover.getPosition().getDirection(), is(Direction.S));
    }

    @Test()
    public void whenRoverFacesNorthAndTurnsLeftThreeTimesRoverShouldFaceEast() throws RoverOutOfBoundsException {
        String[] moves = {"L", "L", "L"};
        rover.moveOnRoute(moves);
        assertThat(rover.getPosition().getDirection(), is(Direction.E));
    }

    @Test()
    public void whenRoverFacesNorthAndTurnsLeftFourTimesRoverShouldFaceNorth() throws RoverOutOfBoundsException {
        String[] moves = {"L", "L", "L", "L"};
        rover.moveOnRoute(moves);
        assertThat(rover.getPosition().getDirection(), is(Direction.N));
    }

    @Test()
    public void whenRoverFacesNorthAndTurnsRightOnceRoverShouldFaceEast() throws RoverOutOfBoundsException {
        String[] moves = {"R"};
        rover.moveOnRoute(moves);
        assertThat(rover.getPosition().getDirection(), is(Direction.E));
    }

    @Test()
    public void whenRoverFacesNorthAndTurnsRightTwiceRoverShouldFaceSouth() throws RoverOutOfBoundsException {
        String[] moves = {"R", "R"};
        rover.moveOnRoute(moves);
        assertThat(rover.getPosition().getDirection(), is(Direction.S));
    }

    @Test()
    public void whenRoverFacesNorthAndTurnsRightThreeTimesRoverShouldFaceWest() throws RoverOutOfBoundsException {
        String[] moves = {"R", "R", "R"};
        rover.moveOnRoute(moves);
        assertThat(rover.getPosition().getDirection(), is(Direction.W));
    }

    @Test()
    public void whenRoverFacesNorthAndTurnsRightFourTimesRoverShouldFaceNorth() throws RoverOutOfBoundsException {
        String[] moves = {"R", "R", "R", "R"};
        rover.moveOnRoute(moves);
        assertThat(rover.getPosition().getDirection(), is(Direction.N));
    }

    @Test()
    public void whenRoverFacesNorthAndMovesForwardOnceYCoordinateShouldBeIncrementedByOne() throws RoverOutOfBoundsException {
        String[] moves = {"M"};
        int yCoord = rover.getPosition().getY();
        rover.moveOnRoute(moves);
        assertThat(rover.getPosition().getY(), is(yCoord + 1));
    }

    @Test()
    public void whenRoverFacesSouthAndMovesForwardOnceYCoordinateShouldBeDecrementedByOne() throws RoverOutOfBoundsException {
        String[] moves = {"L", "L", "M"};
        int yCoord = rover.getPosition().getY();
        rover.moveOnRoute(moves);
        assertThat(rover.getPosition().getY(), is(yCoord - 1));
    }

    @Test()
    public void whenRoverFacesEastAndMovesForwardOnceXCoordinateShouldBeIncrementedByOne() throws RoverOutOfBoundsException {
        String[] moves = {"R", "M"};
        int xCoord = rover.getPosition().getX();
        rover.moveOnRoute(moves);
        assertThat(rover.getPosition().getX(), is(xCoord + 1));
    }

    @Test()
    public void whenRoverFacesWestAndMovesForwardOnceXCoordinateShouldBeDecrementedByOne() throws RoverOutOfBoundsException {
        String[] moves = {"L", "M"};
        int xCoord = rover.getPosition().getX();
        rover.moveOnRoute(moves);
        assertThat(rover.getPosition().getX(), is(xCoord - 1));
    }

    @Test()
    public void whenRoverFacesNorthAndMovesForwardPastMaxyCoordARoverOutOfBoundsExceptionShouldBeThrown() throws RoverOutOfBoundsException {
        String[] moves = {"M", "M", "M", "M"};
        Exception exception = assertThrows(RoverOutOfBoundsException.class, () -> {
            rover.moveOnRoute(moves);
        });
        assertThat(exception.getMessage(), is("Invalid move:  rover reached top edge of plateau."));
    }

    @Test()
    public void whenRoverFacesSouthAndMovesForwardToThePointWhereyCoordIsLessThanZeroARoverOutOfBoundsExceptionShouldBeThrown() throws RoverOutOfBoundsException {
        String[] moves = {"L", "L", "M", "M", "M"};
        Exception exception = assertThrows(RoverOutOfBoundsException.class, () -> {
            rover.moveOnRoute(moves);
        });
        assertThat(exception.getMessage(), is("Invalid move:  rover reached bottom edge of plateau."));
    }

    @Test()
    public void whenRoverFacesEastAndMovesForwardPastMaxxCoordARoverOutOfBoundsExceptionShouldBeThrown() throws RoverOutOfBoundsException {
        String[] moves = {"R", "M", "M", "M", "M"};
        Exception exception = assertThrows(RoverOutOfBoundsException.class, () -> {
            rover.moveOnRoute(moves);
        });
        assertThat(exception.getMessage(), is("Invalid move:  rover reached right edge of plateau."));
    }

    @Test()
    public void whenRoverFacesWestAndMovesForwardToThePointWherexCoordIsLessThanZeroARoverOutOfBoundsExceptionShouldBeThrown() throws RoverOutOfBoundsException {
        String[] moves = {"L", "M", "M", "M", "M"};
        Exception exception = assertThrows(RoverOutOfBoundsException.class, () -> {
            rover.moveOnRoute(moves);
        });
        assertThat(exception.getMessage(), is("Invalid move:  rover reached left edge of plateau."));
    }

}
