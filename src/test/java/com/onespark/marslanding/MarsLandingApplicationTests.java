package com.onespark.marslanding;

import com.onespark.marslanding.exception.InvalidRoverDataException;
import com.onespark.marslanding.exception.RoverOutOfBoundsException;
import com.onespark.marslanding.model.Direction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class MarsLandingApplicationTests {
    private final MarsLandingApplication app = new MarsLandingApplication();
    BufferedReader in = null;

    @BeforeEach()
    public void setup() {
        in = Mockito.mock(BufferedReader.class);
    }

    @Test
    public void whenProcessDataIsCalledAndValidateMaxCoordsReadReturnsFalseProcessDataThrowsAnInvalidRoverDataExceptionWithFormatErrorLine() throws IOException {
        String line1 = "55";
        given(in.readLine()).willReturn(line1);
        Exception exception = assertThrows(InvalidRoverDataException.class, () -> {
            app.processData(in);
        });
        assertThat(exception.getMessage(), is("Data format error at line 1"));
    }

    @Test
    public void whenProcessDataIsCalledAndValidateInitialPositionReadReturnsFalseProcessDataThrowsAnInvalidRoverDataExceptionWithFormatErrorLine() throws IOException {
        String line1 = "5 5";
        String line2 = "1 1 a";
        given(in.readLine()).willReturn(line1).willReturn(line2);
        Exception exception = assertThrows(InvalidRoverDataException.class, () -> {
            app.processData(in);
        });
        assertThat(exception.getMessage(), is("Data format error at line 2"));
    }

    @Test
    public void whenProcessDataIsCalledAndValidateMovesReturnsFalseProcessDataThrowsAnInvalidRoverDataExceptionWithFormatErrorLine() throws IOException {
        String line1 = "5 5";
        String line2 = "1 1 N";
        String line3 = "asdfgf";
        given(in.readLine()).willReturn(line1).willReturn(line2).willReturn(line3).willReturn(null);
        Exception exception = assertThrows(InvalidRoverDataException.class, () -> {
            app.processData(in);
        });
        assertThat(exception.getMessage(), is("Data format error at line 3"));
    }

    @Test
    public void whenProcessDataIsCalledWithValidInputTheRoversMovesGetsProcessedAndFinalPositionGetsPrintedToSTDOut() throws IOException, InvalidRoverDataException, RoverOutOfBoundsException {
        String line1 = "5 5";
        String line2 = "1 1 N";
        String line3 = "MRM";
        final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));
        given(in.readLine()).willReturn(line1).willReturn(line2).willReturn(line3).willReturn(null);
        app.processData(in);
        assertThat(outContent.toString(), is("2 2 E\n"));
    }

}
